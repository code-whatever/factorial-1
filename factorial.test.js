let factorial = require("./factorial")

test("1! produces 1", function() {
    expect(factorial(1)).toBe(1);
})

test("2! produces 2", function() {
    expect(factorial(2)).toBe(2)
})

test("3! produces 3", function() {
    expect(factorial(3)).toBe(6)
})

test("0! produces 1", function() {
    expect(factorial(0)).toBe(1)
})