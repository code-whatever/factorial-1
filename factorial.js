
function factorial(n) {
    let count = n;
    let total = 1;
    while (count > 0) {
        total = total * count
        count -= 1
    }
    return total
}


module.exports = factorial;
